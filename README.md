# Student grading application

The goal of this application is to grade students and show their averge grades. 
On "Add grades" page user enters student's identification number and a grade. Grades are stored on submitting the form. 
To get an average grade user must go "Average grades" page and input a student identification number. 
If the search is successful a student indetification number and average grade are shown.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine. 

### Prerequisites

1. Symfony
2. XAMPP (if on Windows)


### Installing

* [Symfony](https://symfony.com/doc/2.8/setup.html)

*On Linux and macOs*
```
$ sudo mkdir -p /usr/local/bin
$ sudo curl -LsS https://symfony.com/installer -o /usr/local/bin/symfony
$ sudo chmod a+x /usr/local/bin/symfony
```

*On Windows*

```
c:\> php -r "file_put_contents('symfony', file_get_contents('https://symfony.com/installer'));"

c:\> copy symfony c:\xampp\bin\php
c:\> cd c:\xampp\bin\php
c:\> (echo @ECHO OFF & echo php "%~dp0symfony" %*) > symfony.bat

c:\> copy symfony c:\projects
c:\> cd projects
```


* [XAMPP](https://www.apachefriends.org/index.html) - install and make sure to configure and start MySQL module. 

While in desired directory (e.g. c:\projects) clone the repository
```
git clone https://mblaskovic@bitbucket.org/mblaskovic/student-grading-app.git
```

Navigate to cloned repository directory, create database and entities
```
cd student-grading-app
php app/console doctrine:database:create 		
php app/console doctrine:schema:update --force
```


## Running the application

While in cloned repository directory start the server
```
php app/console server:run
```

In your browser navigate to
```
http://localhost:8000/home
```
and you can start using the application


## Built With

* [Symfony](https://symfony.com/) - The web framework used


## Authors

* **Mihovil Blašković**   - [mblaskovic](https://bitbucket.org/mblaskovic/)

