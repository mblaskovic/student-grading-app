<?php

namespace AppBundle\Controller;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\ProsjekOcjena;
use AppBundle\Entity\Student;	

//use Symfony\Component\Form\AbstractType;
//use Symfony\Component\Form\FormBuilderInterface;
//use Symfony\Component\Form\Extension\Core\Type\IntegerType;
//use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Form\UnosOcjeneType;
use AppBundle\Form\UnosProsjekOcjenaType;

class StudentGradingController extends Controller
{


	
	public function startAction(){

		return $this->render('StudentGrading/description.html.twig' );
	}




 	public function addStudentGradesAction(Request $request)
 	{
 		$studentGrade = new Student;

		$form = $this->createForm(UnosOcjeneType::class, $studentGrade );
		$form -> handleRequest($request);


		if ($form->isValid() && $form->isSubmitted()) {
			$studentIdentificationNumber = $form['jmbg']->getData();
            $grade = $form['ocjena']->getData();
            
            $request = Request::create(
    			'student_grades_add',
    			'POST',
    			array('jmbg' => $studentIdentificationNumber, 'ocjena'=>$grade));

 			$this->postStudentGradesAction($request);

 			$this->addFlash('success','Record added! Student jmbg: '.$studentIdentificationNumber.', grade: '.$grade.'.');

		}
		return $this->render('StudentGrading/grade.html.twig', array('form' => $form->createView()));
 	}




 	public function postStudentGradesAction(Request $request)
 	{
 		$em = $this->getDoctrine()->getManager();
        $student=new Student;
        $studentIdentificationNumber = $request->get('jmbg');
        $grade = $request->get('ocjena');

        $student->setJmbg($studentIdentificationNumber);
  		$student->setOcjena($grade);
        $em->persist($student);
  		$em->flush();

  		
  		$averageGrade = new ProsjekOcjena;
  		$studentGradeQuery = $em->createQuery("SELECT s FROM AppBundle:Student s WHERE s.jmbg = ?1")
	        	->setParameter(1, $studentIdentificationNumber)
	        	->getResult();
	    $studentGradeQueryCount = count($studentGradeQuery);

	    $averageGrade->setBrojOcjenjivanja($studentGradeQueryCount);

		$gradeSumQuery = $em->createQuery("SELECT SUM (s.ocjena) FROM AppBundle:Student s WHERE s.jmbg = ?1")
	        ->setParameter(1, $studentIdentificationNumber)
	        ->getSingleScalarResult();

	    $averageGrade->setZbrojOcjena($gradeSumQuery);

	    $getStudent=$em->getRepository('AppBundle:Student')
	    		->findBy(array('jmbg' => $studentIdentificationNumber), array('id' => 'DESC'), 1, 0);
	    
	    foreach ($getStudent as $key => $value) {
			$insertedStudent=$value;
		}

		$averageGrade->setStudentId($insertedStudent);
		$em->persist($averageGrade);
  		$em->flush();
  		return new Response("Grade added!");
  		
 	}



 	public function averageGradesAction(Request $request)
 	{
		$form = $this->createForm(UnosProsjekOcjenaType::class );
		$form -> handleRequest($request);

		if ($form->isValid() && $form->isSubmitted()) {
			
			$studentIdentificationNumber = $form['jmbg']->getData();
			$data = $this->getAverageAction($studentIdentificationNumber)->getContent();
			$jsonData = json_decode($data, true);

        	
        	$jmbg = $jsonData['jmbg'];
        	$avg = $jsonData['avg'];

        	if ($jmbg==false || $avg==false){
        		$this->addFlash('danger','Student not found. Please try again.');;
        	}
        	else{
        		$this->addFlash('success','Search is successful!');
        	}

			return $this->render('StudentGrading/average.html.twig', array(  'form' => $form->createView(), 'studentId'=>$jmbg, 'avg'=>$avg ));

		}

		return $this->render('StudentGrading/average.html.twig', array(  'form' => $form->createView(), 'studentId'=>false, 'avg'=>false  ));
 	}
 	


 	public function getAverageAction($jmbg)
 	{
 		list($studentId,$avg) =$this->calculateAverage($jmbg);
 		$data = array(
            'jmbg' => $studentId,
            'avg' => $avg,
        );
        
  		$response=new JsonResponse($data);
        return $response;
 	}

 	public function calculateAverage($jmbg)
 	{

		$em = $this->getDoctrine()->getManager();
		$studentId=false;
		$avg=false;
		$studentIdentificationNumber=false;

		$searchQuery=$em->createQuery("SELECT u.id FROM AppBundle:Student u WHERE u.jmbg = ?1 ORDER BY u.id DESC")
			->setParameter(1, $jmbg)
			->setMaxResults(1)
			->getOneOrNullResult();
		
		if ($searchQuery){
			foreach ($searchQuery as $key => $value) {
			$studentId=$value;
			}
		}	

		
    	$avgGradesQuery = $em->createQuery("SELECT u FROM AppBundle:ProsjekOcjena u WHERE u.studentId = ?1")
    		->setParameter(1, $studentId)
			->getOneOrNullResult();

		$studentQuery = $em->createQuery("SELECT u FROM AppBundle:Student u WHERE u.id = ?1")
			->setParameter(1, $studentId)
			->getOneOrNullResult();
		

		if ($avgGradesQuery){

			$gradesNumber = $avgGradesQuery->getbrojOcjenjivanja();
			$gradesSum = $avgGradesQuery->getzbrojOcjena();
			$studentIdentificationNumber=$studentQuery->getjmbg();
			$avg = $gradesSum / $gradesNumber;
		}
		
		
		return array($studentIdentificationNumber,$avg);
	}

 	
}




