<?php

namespace AppBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class UnosProsjekOcjenaType extends AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('jmbg', IntegerType::class, array('mapped' => false, 'attr' => array('style'=> 'margin-bottom:15px;margin-left:15px')))
            ->add('save',SubmitType::class, array('label'=>'Search','attr' => array('class'=> 'btn btn-primary', 'style'=>'margin-bottom:15px', 'display'=>'flex')))
        ;


    }
}