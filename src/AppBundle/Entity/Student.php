<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM; 

/**
 * Student
 *
 * @ORM\Table(name="student")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StudentRepository")
 */
class Student
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * 
     * @ORM\Column(name="jmbg", type="bigint" )
     */

    private $jmbg;

    /**
     * @var int
     * 
     * @ORM\Column(name="ocjena", type="smallint",)
     */
    private $ocjena;

    

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set jmbg
     *
     * @param integer $jmbg
     * @return Student
     */
    public function setJmbg($jmbg)
    {
        $this->jmbg = $jmbg;

        return $this;
    }

    /**
     * Get jmbg
     *
     * @return integer 
     */
    public function getJmbg()
    {
        return $this->jmbg;
    }

    /**
     * Set ocjena
     *
     * @param integer $ocjena
     * @return Student
     */
    public function setOcjena($ocjena)
    {
        $this->ocjena = $ocjena;

        return $this;
    }

    /**
     * Get ocjena
     *
     * @return integer 
     */
    public function getOcjena()
    {
        return $this->ocjena;
    }
}
