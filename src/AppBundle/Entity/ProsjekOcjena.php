<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProsjekOcjena
 *
 * @ORM\Table(name="prosjek_ocjena")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProsjekOcjenaRepository")
 */
class ProsjekOcjena
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id; 

    /**
     * @var int
     *
     * @ORM\Column(name="student_id", type="bigint")
     
     */


    //private $studentId;


    /**
    * @ORM\ManyToOne(targetEntity="Student", )
    * @ORM\JoinColumn(name="student_id", referencedColumnName="id",)
    */
    private $studentId;

    /**
     * @var int
     *
     * @ORM\Column(name="broj_ocjenjivanja", type="integer")
     */
    private $brojOcjenjivanja;

    /**
     * @var int
     *
     * @ORM\Column(name="zbroj_ocjena", type="integer")
     */
    private $zbrojOcjena;


    /**
     * @ORM\ManyToOne(targetEntity="Student", inversedBy="prosjek_ocjena")
     * @ORM\JoinColumn(name="student_id", referencedColumnName="jmbg")
     */
    ///private $studentt;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set studentId
     *
     * @param integer $studentId
     * @return ProsjekOcjena
     */
    public function setStudentId($studentId)
    {
        $this->studentId = $studentId;

        return $this;
    }

    /**
     * Get studentId
     *
     * @return integer 
     */
    public function getStudentId()
    {
        return $this->studentId;
    }

    /**
     * Set brojOcjenjivanja
     *
     * @param integer $brojOcjenjivanja
     * @return ProsjekOcjena
     */
    public function setBrojOcjenjivanja($brojOcjenjivanja)
    {
        $this->brojOcjenjivanja = $brojOcjenjivanja;

        return $this;
    }

    /**
     * Get brojOcjenjivanja
     *
     * @return integer 
     */
    public function getBrojOcjenjivanja()
    {
        return $this->brojOcjenjivanja;
    }

    /**
     * Set zbrojOcjena
     *
     * @param integer $zbrojOcjena
     * @return ProsjekOcjena
     */
    public function setZbrojOcjena($zbrojOcjena)
    {
        $this->zbrojOcjena = $zbrojOcjena;

        return $this;
    }

    /**
     * Get zbrojOcjena
     *
     * @return integer 
     */
    public function getZbrojOcjena()
    {
        return $this->zbrojOcjena;
    }
}
